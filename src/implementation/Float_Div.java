package implementation;

import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import utilitytypes.ICpuCore;
import utilitytypes.IModule;

public class Float_Div extends PipelineStageBase {
    public Float_Div(ICpuCore core) {
        super(core, "f_div");
    }
    @Override
    public void compute(Latch input, Latch output) {
        if (input.isNull()) return;
        doPostedForwarding(input);
        InstructionBase ins = input.getInstruction();
        
        if (GlobalData.Fcounter < 15) {
            setResourceWait("resourcename");
            GlobalData.Fcounter++;
            return;
        }
           int result=0;
            //InstructionBase ins = input.getInstruction();
          //  EnumOpcode op=ins.getOpcode();
            int source1 = ins.getSrc1().getValue();
            int source2 = ins.getSrc2().getValue();
            
           
            float src1 = Float.intBitsToFloat(source1);
            float src2 = Float.intBitsToFloat(source2);
            float res = src1 / src2;
            result= Float.floatToRawIntBits(res);
            
            
            GlobalData.Fcounter=0;
            boolean isfloat = ins.getSrc1().isFloat() || ins.getSrc2().isFloat();
           
           // output.setInstruction(ins);
       
    	
    	output.setInstruction(input.getInstruction());
        output.copyAllPropertiesFrom(input);
       // output.setResultFloatValue(result);
        output.setResultValue(result, isfloat);
    }   
    
}