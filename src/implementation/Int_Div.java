package implementation;

import org.w3c.dom.css.Counter;

import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import utilitytypes.ICpuCore;
import utilitytypes.IModule;

public class Int_Div extends PipelineStageBase {
    public Int_Div(ICpuCore icore) {
        super(icore, "div");
    }
    /*private int getSomeExternalResource(int src1, int src2) {
        // Returning -1 means we couldn't get the resource
        return -1;
    }
    
    private int computeSomeResult(int src1, int src2, int external) {
        // This is some computed result
    	int result = src1 / src2;
        return result;
    }*/
		// TODO Auto-generated constructor stub

	@Override
    public void compute(Latch input, Latch output) {
        if (input.isNull()) return;
    
        InstructionBase ins = input.getInstruction();
        doPostedForwarding(input);
        int src1 = ins.getSrc1().getValue();
        int src2 = ins.getSrc2().getValue();
        
        //int external = getSomeExternalResource(src1, src2);
        if (GlobalData.Icounter < 15) {
            setResourceWait("resourcename");
            GlobalData.Icounter++;
            return;
        }
        int result= src1/src2;
        GlobalData.Icounter = 0;
        output.setInstruction(input.getInstruction());
        output.copyAllPropertiesFrom(input);
        output.setResultValue(result);
    }   
    
}