/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import static utilitytypes.IProperties.RESULT_VALUE;

import baseclasses.CpuCore;
import tools.InstructionSequence;
import utilitytypes.IPipeStage;
import utilitytypes.Logger;

/**
 * This is an example of a class that builds a specific CPU simulator out of
 * pipeline stages and pipeline registers.
 * 
 * @author 
 */
public class MyCpuCore extends CpuCore {
    static final String[] producer_props = {RESULT_VALUE};
        
    public void initProperties() {
        properties = new GlobalData();
    }
    
    public void loadProgram(InstructionSequence program) {
        getGlobals().loadProgram(program);
    }
    
    public void runProgram() {
        properties.setProperty("running", true);
        while (properties.getPropertyBoolean("running")) {
            Logger.out.println("## Cycle number: " + cycle_number);
            advanceClock();
        }
    }

    @Override
    public void createPipelineRegisters() {
        createPipeReg("FetchToDecode");
        createPipeReg("DecodeToExecute");
        createPipeReg("DecodeToMemory");
        createPipeReg("DecodeTof_mul");
        createPipeReg("DecodeTof_asc");
        createPipeReg("DecodeTomul");
       
        createPipeReg("DecodeTodiv");
        createPipeReg("divToWriteback");
       
        createPipeReg("ExecuteToWriteback");
        //createPipeReg("MemoryToWriteback");
        
        createPipeReg("DecodeTof_div");
        createPipeReg("f_divToWriteback");
    }

    @Override
    public void createPipelineStages() {
        addPipeStage(new AllMyStages.Fetch(this));
        addPipeStage(new AllMyStages.Decode(this));
        addPipeStage(new AllMyStages.Execute(this));
        addPipeStage(new AllMyStages.Memory(this));
        addPipeStage(new AllMyStages.Writeback(this));
        addPipeStage(new Int_Div(this));
        addPipeStage(new Float_Div(this));
    }

    @Override
    public void createChildModules() {
        addChildUnit(new Float_Mul(this, "f_mul"));
        addChildUnit(new Float_add_sub_cmp(this,"f_asc"));
        addChildUnit(new Int_Mul(this,"mul")); 
        addChildUnit(new Mem_Class(this, "mem"));
    }

    @Override
    public void createConnections() {
        connect("Fetch", "FetchToDecode", "Decode");
        connect("Decode", "DecodeToExecute", "Execute");
       // connect("Decode", "DecodeToMemory", "Memory");
        
        connect("Decode", "DecodeTof_mul", "f_mul");
        connect("Decode", "DecodeTof_asc", "f_asc");
        connect("Decode", "DecodeTomul", "mul");
        
        connect("Decode", "DecodeTodiv","div");
        connect("div","divToWriteback","Writeback");
        
        connect("Decode", "DecodeTof_div","f_div");
        connect("f_div","f_divToWriteback","Writeback");

        connect("Execute","ExecuteToWriteback", "Writeback");
        //connect("Memory", "MemoryToWriteback", "Writeback");
        connect("Decode", "DecodeToMemory", "mem");
        
        connect("f_mul", "Writeback");
        connect("f_asc", "Writeback");
        connect("mul", "Writeback");
        connect("mem", "Writeback");
        
    }

    @Override
    public void specifyForwardingSources() {
        addForwardingSource("ExecuteToWriteback");
       // addForwardingSource("MemoryToWriteback");
        addForwardingSource("divToWriteback");
        addForwardingSource("f_divToWriteback");
        addForwardingSource("f_mul.out");
        addForwardingSource("f_asc.out");
        addForwardingSource("mul.out");
        addForwardingSource("mem.out");
    }

    @Override
    public void specifyForwardingTargets() {
        // Not really used for anything yet
    }

    @Override
    public IPipeStage getFirstStage() {
        // CpuCore will sort stages into an optimal ordering.  This provides
        // the starting point.
        return getPipeStage("Fetch");
    }
    
    public MyCpuCore() {
        super(null, "core");
        initModule();
        printHierarchy();
        Logger.out.println("");
    }
}
