package implementation;

import baseclasses.FunctionalUnitBase;
import baseclasses.InstructionBase;
import baseclasses.Latch;
import baseclasses.PipelineStageBase;
import tools.MultiStageDelayUnit;
import utilitytypes.EnumOpcode;
import utilitytypes.IFunctionalUnit;
import utilitytypes.IModule;

public class Float_add_sub_cmp  extends FunctionalUnitBase {
    public Float_add_sub_cmp(IModule parent, String name) {
        super(parent, name);
    }

    private static class MyMathUnit extends PipelineStageBase {
        public MyMathUnit(IModule parent) {
            // For simplicity, we just call this stage "in".
            super(parent, "in");
//            super(parent, "in:Math");  // this would be fine too
        }
        
        @Override
        public void compute(Latch input, Latch output) {
            if (input.isNull()) return;
            doPostedForwarding(input);
            int result=0;
            InstructionBase ins = input.getInstruction();
            EnumOpcode op=ins.getOpcode();
            int source1 = ins.getSrc1().getValue();
            int source2 = ins.getSrc2().getValue();
            
            if(op==EnumOpcode.FADD)
            {
            float src1 = Float.intBitsToFloat(source1);
            float src2 = Float.intBitsToFloat(source2);
            float res = src1 + src2;
            result= Float.floatToRawIntBits(res);
            }
            else if(op==EnumOpcode.FSUB)
            {
            float src1 = Float.intBitsToFloat(source1);
            float src2 = Float.intBitsToFloat(source2);
            float res = src1 - src2;
            result= Float.floatToRawIntBits(res);

            }
            if (op == EnumOpcode.FCMP ){
            float src1 = Float.intBitsToFloat(source1);
            float src2 = Float.intBitsToFloat(source2);
            float res = src1 - src2;
            result= Float.floatToRawIntBits(res);

            }
            boolean isfloat = ins.getSrc1().isFloat() || ins.getSrc2().isFloat();
            output.setResultValue(result, isfloat);
            output.setInstruction(ins);
        }
    }
    
    @Override
    public void createPipelineRegisters() {
        createPipeReg("MathToDelay");  
    }

    @Override
    public void createPipelineStages() {
        addPipeStage(new MyMathUnit(this));
    }

    @Override
    public void createChildModules() {
        IFunctionalUnit child = new MultiStageDelayUnit(this, "Delay", 5);
        addChildUnit(child);
    }

    @Override
    public void createConnections() {
        addRegAlias("Delay.out", "out");
        connect("in", "MathToDelay", "Delay");
    }

    @Override
    public void specifyForwardingSources() {
        addForwardingSource("out");
    }
}

